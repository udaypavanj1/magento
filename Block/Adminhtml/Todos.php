<?php

class Consultant_Todos_Block_Adminhtml_Todos extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract {

    protected $_itemRenderer;

    public function _prepareToRender() {
        $helper = Mage::helper('consultant_todos');

        $this->addColumn('name', array(
            'style' => 'width:200px',
            'required' => true,
            'class' => 'required-entry',
            'label' => $helper->__('Name'),
        ));

        $this->addColumn('index', array(
            'style' => 'width:100px',
            'required' => true,
            'class' => 'required-entry',
            'label' => $helper->__('Index'),
        ));

        $this->addColumn('link', array(
            'style' => 'width:200px',
            'required' => true,
            'class' => 'required-entry',
            'label' => $helper->__('Link'),
        ));

        $this->addColumn('allowed_for', array(
            'label' => $helper->__('Allowed For'),
            'renderer' => $this->_getRenderer(),
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = $helper->__('Add');
        $this->setExtraParams('style="display: none;"');
    }

    protected function _getRenderer() {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = $this->getLayout()->createBlock(
                    'consultant_todos/adminhtml_config_form_field_allowedfor', '', array('is_render_to_js_template' => true)
            );
        }
        return $this->_itemRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row) {
        $row->setData(
                'option_extra_attr_' . $this->_getRenderer()
                        ->calcOptionHash($row->getData('allowed_for')), 'selected="selected"'
        );
    }

}
