<?php

class Consultant_Todos_Block_Adminhtml_Config_Form_Field_Allowedfor 
    extends Mage_Core_Block_Html_Select
{
    /**
     * Prepare HTML output
     *
     * @return Mage_Core_Block_Html_Select
     */
    public function _toHtml()
    {
//        $this->addOption('', 'Select Allowed For');
        $this->addOption("new", "New Users");
        $this->addOption("all", "All Users");
        
        return parent::_toHtml();
    }

    /**
     * Set field name
     *
     * @param string $value
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
