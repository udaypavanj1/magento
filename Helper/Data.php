<?php

class Consultant_Todos_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TODOS_FILED_XML_PATH = 'general/consultant_todos/todos_field';

    /**
     * Returns un-serialized data of the custom config field one
     *
     * @return array
     */
    public function getConfigTodosFiled()
    {
        $config = Mage::getStoreConfig(self::TODOS_FILED_XML_PATH);

        if (!$config) {
            return array();
        }

        try {
            $config = Mage::helper('core/unserializeArray')->unserialize($config);
        } catch (Exception $exception) {
            Mage::logException($exception);
            $config = array(); // Return an array if failed to un-serialize data
        }

        return $config;
    }
}

